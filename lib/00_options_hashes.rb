def transmogrify (string, options = {})
  defaults = {times:1 , upcase: false, reverse: false}
  options = defaults.merge(options)
  ans = []
  options[:times].times.each do |w|
    ans << string
  end
  if options[:upcase]
    ans = ans.join.upcase
  end
  if options[:reverse]
    ans.reverse!
  end
  ans
end
